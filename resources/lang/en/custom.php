<?php 

return [
	"menu" => [
		"certificates" => "Certificates",
		"upload_data" => "Upload Data",
		"certificate_view" => "View"
	],
	"users" => "Users",
	"app" => "CERTIFICATE GENERATION",
	"sidebar" => [
		"homepage" => "Homepage"
	],
	"page_title" => [
		"dashboard" => "Dashboard"
	]
];