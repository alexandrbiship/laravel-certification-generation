@extends('layouts.app')
@section('title', 'View All Users')
@section('page_title', 'List all Users')
@section('contents')
    <table id="data_list" class="display responsive-table datatable-data_list">
        <thead>
            <tr>
                <th data-field="id">No</th>
                <th data-field="username">Username</th>
                <th data-field="date">Date</th>
                <th data-field="action"> </th>
            </tr>
        </thead>
        <tfoot>
                <tr></tr>
        </tfoot>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{{$user->username}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    <a href="#mdlChangePassword" class="modal-trigger waves-effect waves-light btn red" data-value="{{$user->id}}" onclick="setUserId(this);"><i class="material-icons left">replay</i> reset password</a>
                    <a class="waves-effect waves-light btn green" data-value="{{$user->id}}" onclick="removeUser(this);"><i class="material-icons left">delete</i>DELETE</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
@include('users.fixed_button')
@include('users.modals.create')
@include('users.modals.change_password')
@push('javascript')
<script src="{{asset('plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/pages/table-data.js')}}"></script>
<script>
    function removeUser(user) {
        var userId = $(user).data('value');

        $.ajax({
            url:"{{url('users/remove')}}",
            method:'post',
            data: {id:userId},
            dataType : 'json', 
            success: function(result) {
                console.log(result);
                if(result.status == 'success') {
                     Materialize.toast('SUCCESS!', 3000, 'rounded');
                     $("#data_list").DataTable()
                     .row( $(user).parents('tr') )
                     .remove()
                     .draw();
                } else {
                    Materialize.toast(result.status, 3000, 'rounded');
                }
            }
         });
    }

    function setUserId(user) {
        var userId = $(user).data('value');
        $("#userId").val(userId);
    }
</script>
@endpush