<!-- Modal Structure -->
<div id="mdlChangePassword" class="modal modal-fixed-footer" style="height:40%">
        <div class="modal-content">
            <h4>Reset Password</h4>
            <div class="row">
                    <form id="chagePasswordFrm" class="col s12">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                    <h6 style="color:red"><b> 
                                            @if ($errors->has('password'))
                                                Password is wrong. Min-Length is 5.
                                            @endif
                                        </b>
                                    </h6>
                            </div>
                            <input id="userId" type="hidden" name="userId" value="" />
                            <div class="input-field col s12">
                                    <input id="password_mdl"  type="password" class="validate">
                                    <label for="password_mdl" data-error="wrong" data-success="right" class="">Password</label>
                            </div>
                            <div class="input-field col s12">
                                    <input id="password_conf_mdl" type="password" class="validate">
                                    <label for="password_conf_mdl" data-error="wrong" data-success="right" class="">Password Confirmation</label>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="$('#chagePasswordFrm').submit();" >OK</a>
        </div>
    </div>
    
    @push('javascript')
    <script>
        $(function() {
            if("{{$errors->has('password')}}") {
                $(".modal-createUser").trigger('click');
            }
        });

        $("#chagePasswordFrm").submit(function(e) {
            e.preventDefault();

            var userId = $("#userId").val();
            if(userId == NaN || userId == "") {
                Materialize.toast('Please select the user you want to delete.', 3000, 'rounded');
                return false;
            }
            changePassword(userId);
        })
        function changePassword(userId) {
            var password = $("#password_mdl").val();
            var password_confirmation = $("#password_conf_mdl").val();

            if(password == NaN || password == "" || password_confirmation == "" || password_confirmation == NaN) {
                Materialize.toast('Please input the password.', 3000, 'rounded');
                resetForm();
                return false;
            }
            if(password !==  password_confirmation || password.length < 5 ) {
                Materialize.toast('Please check the password you input again.', 3000, 'rounded');
                resetForm();
                return false;
            }
            $.ajax({
                url:"{{url('users/change_password')}}",
                method:'post',
                data: {id:userId, password, password_confirmation},
                dataType : 'json', 
                success: function(result) {
                    console.log(result);
                    if(result.status == 'success') {
                         Materialize.toast('SUCCESS!', 3000, 'rounded');
                        
                    } else {
                        Materialize.toast(result.status, 3000, 'rounded');
                       
                    }
                    resetForm();
                }
             });
        }

        function resetForm() {
            var password = $("#password_mdl");
            var password_confiramtion = $("#password_conf_mdl");

            password.val("");
            password_confiramtion.val("");
        }
    </script>
    @endpush