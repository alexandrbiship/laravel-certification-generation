<!-- Modal Structure -->
<div id="mdlCreateUser" class="modal modal-fixed-footer" style="height:50%">
    <div class="modal-content">
        <h4>Create new user</h4>
        <div class="row">
                <form id="registerFrm" class="col s12" action="{{route('register')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                                <h6 style="color:red"><b> 
                                        @if ($errors->has('password') || $errors->has('username'))
                                            Username / Password is wrong.
                                        @endif
                                    </b>
                                </h6>
                        </div>
                        <div class="input-field col s12">
                            <input id="username" name="username" type="text" class="validate" required>
                            <label for="username" data-error="wrong" data-success="right" class="">Username</label>
                        </div>

                        <div class="input-field col s12">
                                <input id="password" name="password" type="password" class="validate" required>
                                <label for="password" data-error="wrong" data-success="right" class="">Password</label>
                        </div>
                        <div class="input-field col s12">
                                <input id="password_confirmation" name="password_confirmation" type="password" class="validate" required>
                                <label for="password_confirmation" data-error="wrong" data-success="right" class="">Password Confirmation</label>
                        </div>
                    </div>
                </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat cancel-bg-upload">Cancel</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="$('#registerFrm').submit();" >OK</a>
    </div>
</div>

@push('javascript')
<script>
    $(function() {
        if("{{$errors->has('username')}}"  || "{{$errors->has('password')}}") {
            $(".modal-createUser").trigger('click');
        }
    })
  
</script>
@endpush