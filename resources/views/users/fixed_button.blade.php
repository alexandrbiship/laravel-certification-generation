<div class="fixed-action-btn horizontal click-to-toggle" style="bottom: 45px; right: 24px;">
        
        <a class="btn-floating btn-large red tooltipped" data-position="top" 
        data-delay="50" 
        data-tooltip="Upload Data">
            <i class="material-icons">menu</i>
        </a>
        <ul>
            <li>
                <a class="modal-trigger modal-createUser  btn-floating green tooltipped" 
                   data-position="top" 
                   data-delay="50" 
                   href="#mdlCreateUser"
                   data-tooltip="Create new user">
                   <i class="material-icons">perm_identity</i>
                </a>
            </li>
            {{--  <li>
                <a class="btn-floating blue tooltipped"
                   data-position="top" 
                   data-delay="50" 
                   data-tooltip="Download All Data">
                <i class="material-icons">attach_file</i></a>
            </li>  --}}
        </ul>
</div>