
@extends('layouts.app')

@section('title', trans('custom.sidebar.homepage'))
@section('page_title',trans('custom.page_title.dashboard'))
@section('contents')

<p>Add <code>class="responsive-table"</code> to the table tag to make the table horizontally scrollable on smaller screen widths.</p><br>
    <table class="responsive-table">
        <thead>
            <tr>
                <th data-field="id">Name</th>
                <th data-field="name">Item Name</th>
                <th data-field="price">Item Price</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Alvin</td>
                <td>Eclair</td>
                <td>$0.87</td>
            </tr>
            <tr>
                <td>Alan</td>
                <td>Jellybean</td>
                <td>$3.76</td>
            </tr>
            <tr>
                <td>Jonathan</td>
                <td>Lollipop</td>
                <td>$7.00</td>
            </tr>
            <tr>
                <td>Shannon</td>
                <td>KitKat</td>
                <td>$9.99</td>
            </tr>
        </tbody>
    </table>
    
@endsection