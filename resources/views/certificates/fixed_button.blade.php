<div class="fixed-action-btn horizontal click-to-toggle" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large red tooltipped" data-position="top" 
        data-delay="50" 
        data-tooltip="Upload Data">
            <i class="material-icons">menu</i>
        </a>
        <ul>
            <li>
                <a class="btn-floating green tooltipped" 
                   data-position="top" 
                   data-delay="50"
                   href="{{url('certificates/upload')}}"
                   data-tooltip="Upload background & Excel data">
                   <i class="material-icons">publish</i>
                </a>
            </li>
            <li>
                <a class="btn-floating blue tooltipped"
                   data-position="top" 
                   data-delay="50" 
                   href="{{url('certificates/donwload').'/zip/all'}}"
                   data-tooltip="Download all certificates">
                <i class="material-icons">work</i></a>
            </li>
        </ul>
</div>