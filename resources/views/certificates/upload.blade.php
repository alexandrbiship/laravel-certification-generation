@extends('layouts.app')
@section('title', 'Upload Data')
@section('page_title', 'Upload Excel file and background image')
@section('contents')
<div id="upload_container">
    <div>
        <h3>Background Image</h3>
        <section>
            <form id="backgroundForm">
                <div class="progressBar"></div>
                <div class="file-field input-field" style="margin-top:50px">
                    <div class="btn teal lighten-1">
                        <span>Background Image</span>
                        <input name="background" type="file" accept="image/png,image/jpeg,image/jpg"/>
                    </div>
                    <div class="file-path-wrapper">
                            <input class="file-path validate valid" type="text">
                    </div>
                </div>
            </form>
        </section>
        <h3>Excel Data</h3>
        <section>
            <br>
            <form action="/file-upload"  id="excelForm">
                <div class="file-field input-field" style="margin-top:50px">
                        <div class="btn teal lighten-1">
                            <span>Background Image</span>
                            <input name="excel" type="file" accept="*.xlsx" />
                        </div>
                        <div class="file-path-wrapper">
                                <input class="file-path validate valid" type="text">
                        </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection

@includeIf('certificates.modals.bgUpload')

@push('javascript')

<script src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('plugins/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('js/pages/form-wizard.js')}}"></script>
<script src="{{asset('plugins/image-cropper/cropper.min.js')}}"></script>
<script src="{{asset('js/pages/form-image-crop.js')}}"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
<script>
  
</script>
@endpush
@push('css')
<link href="{{asset('plugins/image-cropper/cropper.min.css')}}" rel="stylesheet">    
 
@endpush