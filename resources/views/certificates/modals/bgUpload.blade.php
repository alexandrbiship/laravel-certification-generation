<!-- Modal Trigger -->
<a class="modal-trigger modal-background" href="#modal_background"></a>

<!-- Modal Structure -->
<div id="modal_background" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Upload Background Image</h4>
        <div class="row">
                <div class="col m8 s12 image-cropboard" style="height:85% !important">
                    <div class="image-crop" style="height:100% !important">
                    <img id="background_origin" src="{{asset('images/crop.jpg')}}" style="max-width:100% !important" alt="">
                    </div>
                </div>
                <div class="col m4 s12">
                    <p><b>Image Preview:</b></p>
                    <div class="img-preview m-t-xs"></div>
                    <div class="m-t-md">
                        <p class="red-text m-t-sm">Note: Be sure the browser supports canvas before call Rotate method.</p>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat cancel-bg-upload">Cancel</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ok-bg-upload">OK</a>
    </div>
</div>
