@extends('layouts.app')
@section('title', 'View All Certificates')
@section('page_title', 'List all certificates')
@section('contents')
    <table id="certificates_list" class="display responsive-table datatable-data_list">
        <thead>
            <tr>
                <th data-field="id">No</th>
                <th data-field="title">Certificate Title</th>
                <th data-field="name">Name</th>
                <th data-field="event">Event</th>
                <th data-field="institution_name">Institution name</th>
                <th data-field="Signer name">Signer name</th>
                <th data-field="action">Excel FileName</th>
                <th data-field="excel filename"></th>
            </tr>
        </thead>
        <tfoot>
            <tr></tr>
        </tfoot>
        <tbody>
            @foreach($data as $value)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{{$value['certificate_title']}}</td>
                <td>{{$value['name']}}</td>
                <td>{{$value['event']}}</td>
                <td>{{$value['institution_name']}}</td>
                <td>{{$value['signer_name']}}</td>
                <td>{{$value['excel_name']}}</td>
                <td>
                    <a href="javascript:void(0)" 
                       data-value="{{url('certificates/view/').'/'.$value['pdf_path']}}" 
                       class="waves-effect waves-light btn red"
                       onclick="showPDF(this);"
                    >
                        <i class="material-icons left">library_books</i> PDF</a>
                     <a class="waves-effect waves-light btn green"
                        href="{{url('certificates/donwload').'/'.$value['pdf_path'].'/1'}}"
                        target="_blank"
                     >
                     <i class="material-icons left">play_for_work</i>DOWNLOAD</a>
                </td>
            </tr>
          @endforeach
        </tbody>
    </table>
@endsection
@includeIf('certificates.fixed_button')

@push('javascript')
<script src="{{asset('plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/pages/table-data.js')}}"></script>
    <script>
            function showPDF(obj) {
                $filpath = $(obj).data('value');
                $.get(
                    $filpath,
                    function(result) {
                       if(result.status == 'success') {
                           window.open(result.url,'_blank');
                       } else {
                           Materialize.toast('The file is non exists. Please try again.', 3000, 'rounded');
                       }
                    },'json'
                );
            }
    </script>
@endpush