<style>
@page { margin: 0px;padding: 0px; }
body { 
    top:0;
    bottom:0;
    margin: 0px; 
    padding: 0px;
    font-family: DejaVu Sans;
}
html {
    margin: 0px;
    padding: 0px;
}
tr {
    text-align: center;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<img class="container" src="{{$background}}" style="position: fixed; bottom: 0px; right: 0px;height:100%;width:100%;z-index:100;" />
<div style="position: fixed; bottom: 0px; right: 0px;height:100%;width:100%;z-index:1000">
    <table style="width:100%">
        <tbody>
            <tr>
                <td height=120 style="font-size:50px"><b>{{$data->certificate_title}}</b></td>
            </tr>
            <tr>
                <td style="font-size:20px;height:50px">{{$data->intro1}}</td>
            </tr>
            <tr>    
                <td style="font-size:20px;color:red;height:50px"><b>{{$data->name}}</b></td>
            </tr>
            <tr>
                <td style="font-size:20px;height:50px">{{$data->intro2}}</td>
            </tr>
            <tr>
                <td style="font-size:20px;color:red;height:50px">{{$data->event}}</td>
            </tr>
            <tr>
                <td style="font-size:20px;color:red;height:50px">{{$data->date}}</td>
            </tr>
            <tr>
                <td style="font-size:20px;color:red;height:50px">{{$data->hrs}}</td>
            </tr>
            <tr>
                <td height=80 valign=bottom style=""><b>{{$data->institution_name}}</b></td>
            </tr>
            <tr>
                <td valign=bottom style="font-size:20px;height:60px">{{$data->signer_name}}</td>
            </tr>
        </tbody>
    </table><br><br><br>
    <div style="margin-left:20px;font-size:15px;text-align:left"><b>Ref No.</b>{{$serialNo}}</div>
</div>