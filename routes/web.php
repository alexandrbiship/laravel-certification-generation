<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return redirect('/login');
});

Route::get('/certificates/lists', 'CertificateController@showLists');
Route::get('/certificates/upload', function () {
    return view('certificates.upload');
});
Route::get('/certificates/view/{pdfName}', 'CertificateController@showPDF');
Route::get('/certificates/donwload/{pdfName}/{flag}', 'CertificateController@downloadPDF');
// Route::get('/users/list')
Route::post('/certificates/upload', 'CertificateController@upload');
Route::get('/users/lists', 'UserController@index')->name('users');
Route::post('/users/remove', 'UserController@remove');
Route::post('/users/change_password', 'UserController@changePassword');
Route::get('/home', 'HomeController@index')->name('home');
