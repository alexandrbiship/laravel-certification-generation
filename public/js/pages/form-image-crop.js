$(document).ready(function(){

    var blobUrl= null;
    $image = $("#background_origin");
    resizeCropper();
    // get selected image's detail
    
    $('input[name=background]').change(function(e){
        var filetype = this.files[0].name.split('.').pop();
        var filesize = parseInt(this.files[0].size / (1024 * 1024)) ;
        
        if(filesize > 5) {
            Materialize.toast('Filesize must be less than 5MB.', 3000, 'rounded');
            return false;
        }
        if(filetype !="jpg" && filetype != "png" && filetype != "jpeg") {
            Materialize.toast('Filetype is not supported.', 3000, 'rounded');
            return false;
        }
        blobUrl = URL.createObjectURL(this.files[0])
            $("#background_origin").attr('src', blobUrl);
            //$("#replaceWith").val(blobUrl);
            
            $image.cropper('destroy')
            $image.cropper({
                aspectRatio: 4/3,
                preview: ".img-preview",
                viewMode:1,
                dragCrop: false,
            });

            $("#clear").click(function() {
                $image.cropper('clear');
            });

            var $replaceWith = $('#replaceWith');
            $('#replace').click(function () {
            $image.cropper('replace', $replaceWith.val());
            });

            // open modal
            $(".modal-background").trigger('click');

           
    });

    function resizeCropper() {
        var bgModal = $("#modal_background");
        var imageCrop = $(".image-crop");
        imageCrop.css({"height":bgModal.height() * 0.7, "width":bgModal.width()*0.5,"position":"relative"});
    }

    $(".ok-bg-upload").click(function() {
        var form = $("#upload_container");
        form.children("div").steps("next");
       
    //    $image.cropper("getCroppedCanvas").toBlob(function (blob) {

    //      var formData = new FormData();
    //      formData.append('croppedImage', blob);
    //      var progressBar = $(".progressBar");
       
    //    $.ajax({
    //         header:{

    //         },
    //         url:'upload',
    //         method:'post',
    //         data: formData,
    //         processData: false,
    //         contentType: false,
    //         dataType : 'json', 
    //         beforeSend : function() {
    //             progressBar.css('width',0);
    //         },
    //         uploadProgress : function(event, position, totoal, percentComplete) {
    //             progressBar.width(percentComplete + '%');
    //             console.log('percent',percentComplete)
    //         },
    //         complete: function(xhr) {
    //             if(xhr.status == 200 && xhr.statusText == 'OK') {
    //                  Materialize.toast('Upload completed!', 3000, 'rounded');
    //             } else {
    //                 Materialize.toast('Something went wrong. Please try again.', 3000, 'rounded');
    //             }
    //         }
    //      });

    //      $("#backgroundForm").submit(function(e) {
    //          e.preventDefault();

    //      })
    //    })
    })
    
});


