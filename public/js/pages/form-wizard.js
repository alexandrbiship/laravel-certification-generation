$( document ).ready(function() {
    var form = $("#upload_container");
    var formData = new FormData();

    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        autoFocus: true,
        onStepChanging: function (event, currentIndex, newIndex)
        {
           return true
        },
        onFinishing: function (event, currentIndex)
        {
           return true
        },
        onFinished: function (event, currentIndex)
        {
              $image = $("#background_origin");
              $image.cropper("getCroppedCanvas").toBlob(function (blob) {
              formData.append('croppedImage', blob);
              var backgroundCHK = false;
              var excelCHK = false;

              for (var value of formData.entries()) {
                 console.log(value[0], value[1]); 
                if(value[0] =="croppedImage" && value[1]) {
                   backgroundCHK = true;
                } else if(value[0] =="excel" && value[1]) {
                    excelCHK = true;
                }
              }

              if( !backgroundCHK || !excelCHK) {
                Materialize.toast('Please select the file you want to upload', 3000, 'rounded');
                return false;
              }
              $("body").removeClass('loaded');
              $("loader").show();
                $.ajax({
                        url:'upload',
                        method:'post',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType : 'json', 
                        
                        uploadProgress : function(event, position, totoal, percentComplete) {
                            //progressBar.width(percentComplete + '%');
                            console.log('percent',percentComplete)
                        },
                        success: function(result) {
                            $("body").addClass('loaded');
                            $("loader").hide();
                            if(result.status == 'success') {
                                Materialize.toast('Upload completed!', 3000, 'rounded');
                                location.reload();
                            } else {
                                Materialize.toast(result.status, 3000, 'rounded');
                                location.reload();
                            }
                        }
                });
            },'image/jpeg','0.2');
        }
    });
    
    $(".wizard .actions ul li a").addClass("waves-effect waves-blue btn-flat");
    $(".wizard .steps ul").addClass("tabs z-depth-1");
    $(".wizard .steps ul li").addClass("tab");
    $('ul.tabs').tabs();
    $('select').material_select();
    $('.select-wrapper.initialized').prev( "ul" ).remove();
    $('.select-wrapper.initialized').prev( "input" ).remove();
    $('.select-wrapper.initialized').prev( "span" ).remove();
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    // set excel file upload
    $('input[name=excel]').change(function(e){
        var filetype = this.files[0].name.split('.').pop();
        var filesize = parseInt(this.files[0].size / (1024 * 1024)) ;
        
        if(filesize > 5) {
            Materialize.toast('Filesize must be less than 5MB.', 3000, 'rounded');
            return false;
        }
        if(filetype !="xlsx" && filetype != "xls") {
            Materialize.toast('Filetype is not supported.', 3000, 'rounded');
            return false;
        }
        formData.append('excel', this.files[0]);
    });
});