<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->paginate(15);

        return view('users.lists' ,['users' => $users]);
    }
    
    public function remove(Request $request) {
        $id = $request->id;
        if(Auth::user()->is_admin == '1') {
         $users = DB::table('users')->where('id', $id)->delete();

         return json_encode(['status'=>'success']);
        } 
         return json_encode(['status' => 'Permission denided.']);
    }
    public function changePassword(Request $request) {
        $password = $request->password;
        $confirm = $request->password_confirmation;
        $id = $request->id;

        if($password != $confirm || !isset($id)) {
            return json_encode(['status' => 'Please check the password again.']);
        }
        $update = DB::table('users')->where('id', $id)->update(['password'=>bcrypt($password)]);

        if($update) {
            return json_encode(['status' => 'Success!']);
        } else {
            return json_encode(['status' => 'Something went wrong.']);
        }
    }
}
