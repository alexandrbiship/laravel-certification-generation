<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use \PDF;

class CertificateController extends Controller
{   
   private $dirName;
   private $imageHashName;
   private $excelHashName;
   private $data;
   public function __construct() {
        $this->middleware('auth');
   }

   public function upload(Request $request)
   {
       
        $image = $request->file('croppedImage');
        $excel = $request->file('excel');
        $msg = [];

       if($request->hasFile('croppedImage') && $request->hasFile('excel')) {
           
        $dirName = date('y-m-d-h');
        $this->dirName = $dirName;

        $originalExcelName = $excel->getClientOriginalName();
        // get hashed name to save database;
        $this->imageHashName = $imageHashName = $image->hashName();
        $this->excelHashName = $excelHashName = $excel->hashName();

        $this->data = array(
            "bg_path" => $dirName.'/'.$imageHashName,
            "excel_path" => $dirName.'/'.$excelHashName,
            "excel_name" => $originalExcelName,
            "user_id" => Auth::user()->id
        );

            $image->store($dirName);
            $excel->store($dirName);
            $checked = false;
            // check excel file format is correct
            $savedExcelPath = 'public/upload/'.$dirName.'/'.$excelHashName;
            $row = Excel::load($savedExcelPath);
                $row = $row->first()->toArray();
               
                $columns = ["certificate_title", "intro1", "name" , "intro2", "event", "date", "hrs", "institution_name", "signer_name"];
                foreach($columns as $value) {                    
                    if(!isset($row[$value])){
                        $checked = false;
                    // delete stored file
                       unlink(public_path('upload/'.$this->dirName.'/'.$this->excelHashName));
                       unlink(public_path('upload/'.$this->dirName.'/'.$this->imageHashName));
                       break;
                    } 
                    $checked = true;
                }
                if($checked) {
                    $result = DB::table('uploads')->insert($this->data);
                    if($result) {
                        $savePdf = $this->savePdf($dirName.'/'.$excelHashName);
                        if($savePdf == false) {
                            $msg = ['status' => 'There is an error to save PDF.'];
                        } else {
                            $msg = ['status' => 'success'];
                        }
                    } else {
                        $msg = ['status' => 'Something went wrong. Please try again'];
                    }
                } else {
                    $msg = ['status' => 'Please check your excel file columns'];
                }
                $this->resAjax($msg);
       }
   }

   public function showLists(Request $request) {

    $userId = Auth::user()->id;

    if(Auth::user()->is_admin == '1') {
        $result = DB::table('uploads')->get();
    } else {
        $result = DB::table('uploads')->where('user_id',$userId)->get();
    }
    $resData = array();

    foreach($result as $index => $field) {
        $savedExcelPath = public_path('upload').'/'.$field->excel_path;
        // dd($savedExcelPath);
        $row = Excel::load($savedExcelPath)->get();
        foreach($row->first->toArray() as $key => $data) {
            $resData[$index][$key] = $data;
            $resData[$index]['excel_name'] = $field->excel_name;
            $resData[$index]['id'] = $field->id;
            $resData[$index]['excel_path'] = $field->excel_path;
            $resData[$index]['pdf_path'] = $field->pdf_path;
        }    
    }
      return view('certificates/lists', ['data' => $resData]);
   }

   public function resAjax($msg) {
       
        echo json_encode($msg);
   }

   public function savePdf($filePath) {
       
        $result = DB::table('uploads')->where('excel_path', $filePath)->where('user_id', Auth::user()->id)->first();
        $image = $result->bg_path;
        $row = Excel::load(public_path('upload').'/'.$filePath)->get()->first();
        $serialNo = date('ymdhis');
        $html = view('certificates.pdf',['background' => asset('upload/'.$image),'data'=>$row,'serialNo'=>$serialNo])->render();

        // save database PDF file name
        $update = DB::table('uploads')->where('excel_path', $filePath)->where('user_id', Auth::user()->id)->update(['pdf_path'=>$serialNo.'.pdf']);
        
        if($update) {
            return PDF::loadHTML($html)->setPaper('A4', 'landscape')->setWarnings(false)->save(public_path('upload').'/'.$serialNo.'.pdf');
        } else {
            return false;
        }
   }

   public function showPDF(Request $reqeust, $pdfName) {

        $result = DB::table('uploads')->where('pdf_path', $pdfName)->first();

        if(!empty($result)) {
            return json_encode(['status'=> 'success', 'url' => asset('upload').'/'.$result->pdf_path]);
        }
   }
   public function downloadPDF (Request $request, $pdfName, $flag) {

        $currentUser = Auth::user();
        $isAdmin = $currentUser->is_admin == '1' ? true : false;
        $headers = [
            'Content-Type' => 'application/pdf'
        ];
        if($flag == "all") {

            $zipper = new \Chumper\Zipper\Zipper;
            $zipfilePath = public_path('upload/zip').'/'.$currentUser->id.'-certificates.zip';
            $zipper->zip($zipfilePath)->folder('certificates');

            if($isAdmin) {
                $pdfs = DB::table('uploads')->get()->toArray();
            } else {
                $pdfs = DB::table('uploads')->where('user_id', $currentUser->id)->get()->toArray();
            }
            foreach($pdfs as $key => $value) {
                $zipper->folder('certificates')->add(public_path('upload').'/'.$value->pdf_path);
            }

            $zipper->close();
            return response()->download($zipfilePath, 'certificates.zip', ['Content-Type'=>'application/zip']);
            
        } else {
            $filePath = public_path('upload').'/'.$pdfName;
            
            if($isAdmin) {
                $excelName = DB::table('uploads')->where('pdf_path', $pdfName)->first()->excel_name;
            } else {
                $excelName = DB::table('uploads')->where('pdf_path', $pdfName)->where('user_id', $currentUser->id)->first()->excel_name;
            }
            return response()->download($filePath, explode('.',$excelName)[0].'.pdf', $headers);
        }
   }
}
